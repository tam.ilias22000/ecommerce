FROM eclipse-temurin:11
RUN mkdir /opt/app
COPY target/emarket-0.0.1-SNAPSHOT.jar /opt/app
CMD ["java", "-jar", "/opt/app/emarket-0.0.1-SNAPSHOT.jar"]
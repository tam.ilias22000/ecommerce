package com.example.emarket.it;

import com.example.emarket.dto.CategorieDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

@IntegrationTest
public class CategorieIT {

    @Autowired
    private TestRestTemplate restTemplate;

    //@Autowired
    private EntitiesPersister entitiesPersister;

    @Test
    void shouldCreateCategorie(){
        CategorieDto inputDto = new CategorieDto();
        inputDto.setName("voiture");

        ResponseEntity<CategorieDto> response = restTemplate.postForEntity("/categories", inputDto, CategorieDto.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        assertThat(response.getBody().getId()).isNotNull();

        ResponseEntity<CategorieDto> getResponse = restTemplate.getForEntity(
                URI.create("/categories/" + response.getBody().getId()),
                CategorieDto.class);

        assertThat(getResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(getResponse.getBody())
                .returns(inputDto.getName(), CategorieDto::getName);
    }

}

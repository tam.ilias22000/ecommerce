package com.example.emarket.it;

import com.example.emarket.entity.Categorie;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class EntitiesPersister {

    private static final List<Class<?>> ENTITIES_PERSISTENCE_ORDER = List.of(
            Categorie.class
    );
    private final EntityManager entityManager;


    public EntitiesPersister(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    @Transactional
    public void persist(Object...entities){
        Arrays.stream(entities)
                .sorted(Comparator.comparing(entity ->ENTITIES_PERSISTENCE_ORDER.indexOf(entity.getClass())))
                .forEach(entityManager::persist);
    }
}

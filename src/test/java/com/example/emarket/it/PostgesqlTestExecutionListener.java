package com.example.emarket.it;

import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.TestExecutionListener;
import org.testcontainers.containers.PostgreSQLContainer;

import javax.sql.DataSource;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class PostgesqlTestExecutionListener implements TestExecutionListener {
    private final Logger LOGGER = LoggerFactory.getLogger(PostgesqlTestExecutionListener.class);
    private static final PostgreSQLContainer<?> postgresqlContainer;

    public static final String DB_NAME = "categorie_db";
    public static final String DB_USERNAME = "categorie";
    public static final String DB_PASSWORD = "password";

    public static final ResourceDatabasePopulator DB_CLEANUP_SCRIPT = new ResourceDatabasePopulator(new ClassPathResource("truncate.sql"));

    static {
        // Start a new Postgres container
        postgresqlContainer = new PostgreSQLContainer<>("postgres:13.4-alpine")
                .withDatabaseName(DB_NAME)
                .withUsername(DB_USERNAME)
                .withPassword(DB_PASSWORD)
                .withTmpFs(Map.of("/var/lib/postgresql/data", ""));
        postgresqlContainer.start();

        // Override Spring properties to use the new Postgres instance
        System.setProperty("spring.datasource.url", postgresqlContainer.getJdbcUrl());
        System.setProperty("spring.datasource.username", DB_USERNAME);
        System.setProperty("spring.datasource.password", DB_PASSWORD);
    }

    @Override
    public void beforeTestMethod(TestContext testContext) {
        LOGGER.info("Initialize database before test...");
        DataSource postgresDatasource = testContext.getApplicationContext().getBean(DataSource.class);
        DB_CLEANUP_SCRIPT.execute(postgresDatasource);
    }

}

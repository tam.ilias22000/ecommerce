


create table if not exists categoriedeproduits (
                                     id integer generated always as identity primary key,
                                     name text not null
);
create index if not exists categoriedeproduits_name_idx on categoriedeproduits (name);

create table if not exists produits
(
    id integer generated always as identity primary key,
    name text ,
    price integer,
    description text ,
    categoriedeproduits_id integer references categoriedeproduits(id)

);
create  index if not exists produits_name_idx on produits (name);
create  index if not exists produits_price_idx on produits (price);
create  index if not exists produits_description_idx on produits (description);
create  index if not exists produits_categoriedeproduits_id_idx on produits (categoriedeproduits_id);


create table if not exists carts (
    id integer generated always as identity primary key ,
    creation timestamp not null default now()
);

create index if not exists carts_creation_idx on carts (creation);

create table if not exists cart_lines (
    cart_id integer not null references carts(id),
    produit_id integer not null references produits(id),
    quantity integer not null ,
    primary key (cart_id, produit_id)
);
create table if not exists custumers (
    id integer generated always as identity primary key ,
    first_name text,
    last_name text,
    email text
);



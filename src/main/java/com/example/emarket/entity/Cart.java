package com.example.emarket.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "carts")

public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ElementCollection
    @CollectionTable(name = "cart_lines",
    joinColumns = @JoinColumn(name = "cart_id", referencedColumnName = "id"))
    private List<CartLine> cartLineSet = new ArrayList<>();
    @Column(name = "creation")
    private Instant dateDeCreation = Instant.now();

    public Cart() {
    }

    public Cart(Integer id, List<CartLine> cartLineSet, Instant dateDeCreation) {
        this.id = id;
        this.cartLineSet = cartLineSet;
        this.dateDeCreation = dateDeCreation;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CartLine> getCartLineSet() {
        return cartLineSet;
    }

    public void setCartLineSet(List<CartLine> cartLineSet) {
        this.cartLineSet = cartLineSet;
    }

    public Instant getDateDeCreation() {
        return dateDeCreation;
    }

    public void setDateDeCreation(Instant dateDeCreation) {
        this.dateDeCreation = dateDeCreation;
    }
}

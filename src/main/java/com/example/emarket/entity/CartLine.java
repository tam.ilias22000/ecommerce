package com.example.emarket.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable

public class CartLine {

    @ManyToOne
    @JoinColumn(name = "produit_id", referencedColumnName = "id")
    private Product product;
    private Integer quantity;


    public CartLine(Product product, Integer quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public CartLine() {

    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}

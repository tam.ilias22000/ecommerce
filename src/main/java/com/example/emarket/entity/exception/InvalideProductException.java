package com.example.emarket.entity.exception;

public class InvalideProductException extends InvalidException{
    public InvalideProductException(){

    }
    public InvalideProductException(String message){
        super(message);
    }
    public InvalideProductException(String message, Throwable cause) {
        super(message, cause);
    }
    public InvalideProductException(Throwable cause) {
        super(cause);
    }
    public InvalideProductException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

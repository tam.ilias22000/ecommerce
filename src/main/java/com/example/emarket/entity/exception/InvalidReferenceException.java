package com.example.emarket.entity.exception;

public class InvalidReferenceException extends InvalidException {
    private final Integer id;
    private final Class<?> entityClass;

    public InvalidReferenceException(Class<?> entityClass, Integer id) {
        super(entityClass.getSimpleName() + " reference " + id + " is invalid");
        this.id = id;
        this.entityClass = entityClass;
    }

    public InvalidReferenceException(Class<?> entityClass, Integer id, Throwable cause) {
        super(entityClass.getSimpleName() + " reference " + id + " is invalid", cause);
        this.id = id;
        this.entityClass = entityClass;
    }

    public Integer getId() {
        return id;
    }

    public Class<?> getEntityClass() {
        return entityClass;
    }

}

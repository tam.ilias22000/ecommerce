package com.example.emarket.entity.exception;

public class InvalidBasketException extends InvalidException{
    public InvalidBasketException() {
    }

    public InvalidBasketException(String message) {
        super(message);
    }

    public InvalidBasketException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidBasketException(Throwable cause) {
        super(cause);
    }

}

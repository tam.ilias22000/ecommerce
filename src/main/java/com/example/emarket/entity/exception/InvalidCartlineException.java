package com.example.emarket.entity.exception;

public class InvalidCartlineException extends InvalidException {
    public InvalidCartlineException() {
    }

    public InvalidCartlineException(String message) {
        super(message);
    }

    public InvalidCartlineException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCartlineException(Throwable cause) {
        super(cause);
    }

    public InvalidCartlineException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

package com.example.emarket.repository;

import com.example.emarket.entity.CartLine;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartLineRepository {
    List<CartLine> findAll();
}

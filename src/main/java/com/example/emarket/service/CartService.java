package com.example.emarket.service;

import com.example.emarket.entity.Cart;
import com.example.emarket.entity.CartLine;
import com.example.emarket.entity.Product;
import com.example.emarket.repository.CartRepository;
import com.example.emarket.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class CartService {

    private final CartRepository cartRepository;
    private final ProductRepository productRepository;

    public CartService(CartRepository cartRepository, ProductRepository productRepository) {
        this.cartRepository = cartRepository;
        this.productRepository = productRepository;
    }


    public List<Cart> findAllCarts(){
        return cartRepository.findAll();
    }

    @Transactional
    public Cart findCartById(Integer id){
        return cartRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("cart find By Id n'existe pas "));
    }

    @Transactional
    public void deleteCart(Integer id){
        cartRepository.delete(findCartById(id));
    }

    @Transactional
    public Cart addCart(){
        Cart cart = new Cart();
        return cartRepository.save(cart);
    }

    @Transactional
    public Cart addProductintoCart(Integer cartId, Integer productId, Integer quantity){

        Cart cart = cartRepository
                .findById(cartId)
                .orElseThrow(() -> new IllegalArgumentException("cart n'existe pas"));

        Product product = productRepository
                .findById(productId)
                .orElseThrow(() ->new IllegalArgumentException("produit non trouver"));

        CartLine cartLine = new CartLine(product,quantity);
        cart.getCartLineSet().add(cartLine);
        return cart;
    }

    @Transactional
    public void deleteProductIntocart(Integer cartId, Integer productId){
        Cart cart = cartRepository
                .findById(cartId)
                .orElseThrow(() -> new IllegalArgumentException("cart n'existe pas"));

        Product product = productRepository
                .findById(productId)
                .orElseThrow(() ->new IllegalArgumentException("produit non trouver"));

        cart.getCartLineSet().removeIf(cartLine -> Objects.equals(cartLine.getProduct(),product));

    }

}

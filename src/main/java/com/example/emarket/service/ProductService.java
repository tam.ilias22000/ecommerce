package com.example.emarket.service;

import com.example.emarket.entity.Product;
import com.example.emarket.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> findAllProduct(){
        return productRepository.findAll();
    }

    @Transactional
    public Product createProduct(Product product){
        productRepository.save(product);
        return product;
    }

    @Transactional
    public Product getProductById(Integer id){
        return productRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("produit n'existe pas"));
    }

    @Transactional
    public Product updateProduct(Product newProduct, Integer id){
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("product not found to update"));
        newProduct.setName(product.getName());
        return newProduct;
    }

    @Transactional
    public void deleteProduct(Integer id){
        productRepository.delete(getProductById(id));
    }


}

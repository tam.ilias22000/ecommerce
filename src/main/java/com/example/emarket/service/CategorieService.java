package com.example.emarket.service;

import com.example.emarket.entity.Categorie;
import com.example.emarket.repository.CategorieRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CategorieService {

    private final CategorieRepository categorieRepository;

    public CategorieService(CategorieRepository categorieRepository) {
        this.categorieRepository = categorieRepository;
    }

    public List<Categorie> findAllCategories(){
        return categorieRepository.findAll();
    }

    @Transactional
    public Categorie createCategorie(Categorie categorie){

        return categorieRepository.save(categorie);
    }
}

package com.example.emarket.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.emarket.entity.exception.FunctionalException;
import com.example.emarket.entity.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionsHandler.class);
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorDto> onNotFoundException(NotFoundException e){

        LOGGER.debug("Not found exception handling" , e);
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ErrorDto(e.getMessage()));
    }

    @ExceptionHandler(FunctionalException.class)
    public ResponseEntity<ErrorDto> onFunctionalException(FunctionalException e){
        LOGGER.debug("Not found exception handling" , e);
        return ResponseEntity.badRequest()
                .body(new ErrorDto(e.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDto> onUnexpectedexception(Exception e){
        LOGGER.debug("Not found exception handling" , e);
        return ResponseEntity.internalServerError()
                .body(new ErrorDto("Unexpected error"));
    }
}

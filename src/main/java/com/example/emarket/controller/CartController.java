package com.example.emarket.controller;

import com.example.emarket.dto.CartDto;
import com.example.emarket.dto.CategorieDto;
import com.example.emarket.entity.Cart;
import com.example.emarket.repository.CartRepository;
import com.example.emarket.service.CartService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("baskets")
@CrossOrigin
public class CartController {

    private final CartService cartService;
    private final CartRepository cartRepository;


    public CartController(CartService cartService, CartRepository cartRepository) {
        this.cartService = cartService;
        this.cartRepository = cartRepository;
    }

    @GetMapping("")
    public List<CartDto> findCart(){
        return cartRepository
                .findAll().stream()
                .map(x->CartDto.fromDomain(x))
                .collect(Collectors.toList());

    }

    @GetMapping("/{id}")
    public CartDto findCartDtoById(@PathVariable Integer id){
        return CartDto.fromDomain(cartService.findCartById(id));

    }

    @DeleteMapping("/{id}")
    public void deletecart(@PathVariable Integer id){
        cartService.deleteCart(id);
    }

    @PostMapping("")
    public CartDto addcart(){

        return CartDto.fromDomain(cartService.addCart());
    }



    @PostMapping("/{cartId}/{productid}")
    public CartDto addProductintocart(@PathVariable Integer cartId,
                                   @PathVariable Integer productid,
                                   @RequestParam Integer quantity){

        return CartDto.fromDomain(cartService.addProductintoCart(cartId,productid,quantity));
    }

    @DeleteMapping("/{cartId}/{productid}")
    public void deleteProductFromCart(@PathVariable Integer cartId,
                                 @PathVariable Integer productid){
        cartService.deleteProductIntocart(cartId,productid);

    }
}

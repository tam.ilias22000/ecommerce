package com.example.emarket.controller;

import com.example.emarket.dto.PageDto;
import com.example.emarket.dto.ProductDto;
import com.example.emarket.entity.Product;
import com.example.emarket.repository.ProductRepository;
import com.example.emarket.service.ProductService;
import org.springframework.data.web.SortDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Pageable;
import javax.persistence.criteria.CriteriaBuilder;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("products")
@CrossOrigin
public class ProductController {

    private final ProductService productService;
    private final ProductRepository productRepository;


    public ProductController(ProductService productService, ProductRepository productRepository) {
        this.productService = productService;
        this.productRepository = productRepository;
    }


    public List<Product> findAllProduct(){
        return productService.findAllProduct();
    }

    @GetMapping("")
    public List<ProductDto>findAllProducts(){
        return productRepository.findAll()
                .stream()
                .map(x ->ProductDto.fromDomain(x))
                .collect(Collectors.toList());
    }

    //pagination
    //@GetMapping("")
    public PageDto<ProductDto> getProducts(@SortDefault(value = {"id"}) Pageable pageable){
        return PageDto.fromDomain(productRepository.findAll(pageable), ProductDto::fromDomain);
    }


    @PostMapping("")
    public ProductDto createProduct(@RequestBody ProductDto productDto){

        return ProductDto.fromDomain(productService.createProduct(ProductDto.toDomain(productDto)));
    }



    @GetMapping("/{id}")
    public ProductDto findProductById(@PathVariable Integer id){
        return ProductDto.fromDomain(productService.getProductById(id));
    }

    @PutMapping("/{id]")
    public ProductDto updateProduct(ProductDto productDto, Integer id){

        return ProductDto.fromDomain(productService.updateProduct(ProductDto.toDomain(productDto),id));
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Integer id){
        productService.deleteProduct(id);
    }
}

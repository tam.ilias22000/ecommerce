package com.example.emarket.controller;

import java.time.Instant;

public class ErrorDto {

    private  Instant timestamp = Instant.now();
    private  String message;


    public ErrorDto(String messsage) {
        this.message = messsage;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getMesssage() {
        return message;
    }

    public ErrorDto() {
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }


    public void setMessage(String message) {
        this.message = message;
    }

}

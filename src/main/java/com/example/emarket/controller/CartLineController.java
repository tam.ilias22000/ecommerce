package com.example.emarket.controller;

import com.example.emarket.dto.CartLineDto;
import com.example.emarket.entity.CartLine;
import com.example.emarket.repository.CartLineRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin
public class CartLineController {

    private final CartLineRepository cartLineRepository;

    public CartLineController(CartLineRepository cartLineRepository) {
        this.cartLineRepository = cartLineRepository;
    }

    @GetMapping("productlines")
    public List<CartLineDto> findAllCartLine(){
        return cartLineRepository
                .findAll()
                .stream()
                .map(x->CartLineDto.fromDomain(x))
                .collect(Collectors.toList());
    }
}

package com.example.emarket.controller;

import com.example.emarket.dto.CartDto;
import com.example.emarket.dto.CategorieDto;
import com.example.emarket.entity.Categorie;
import com.example.emarket.repository.CategorieRepository;
import com.example.emarket.service.CategorieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("categories")
@CrossOrigin
public class CategorieController {

    private final CategorieService categorieService;
    private final CategorieRepository categorieRepository;

    public CategorieController(CategorieService categorieService, CategorieRepository categorieRepository) {
        this.categorieService = categorieService;
        this.categorieRepository = categorieRepository;
    }

    @GetMapping("")
    public List<CategorieDto> findAllCategorie(){
        return categorieService
                .findAllCategories()
                .stream()
                .map(x -> CategorieDto.fromDomain(x))
                .collect(Collectors.toList());
    }
    //@PostMapping("")
    public CategorieDto createcategorie(@RequestBody Categorie categorie){
        //return CategorieDto.fromDomain(categorieService.createCategorie(CategorieDto.toDomain(categorieDto)));
        return CategorieDto.fromDomain(categorieService.createCategorie(categorie));
    }

    @PostMapping
    public ResponseEntity<CategorieDto> createCategorie(CategorieDto categorieDto){
        Categorie newCategorie = categorieService.createCategorie(categorieDto.toDomain());

        URI location = URI.create("/categories/" + newCategorie.getId());
        return ResponseEntity.created(location)
                .body(CategorieDto.fromDomain(newCategorie));
    }
}

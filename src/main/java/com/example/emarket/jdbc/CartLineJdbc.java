package com.example.emarket.jdbc;

import com.example.emarket.entity.CartLine;
import com.example.emarket.entity.Product;
import com.example.emarket.repository.CartLineRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
@Component
public class CartLineJdbc implements CartLineRepository {

    private final JdbcTemplate jdbcTemplate;

    public CartLineJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public static RowMapper<CartLine> rowMapper= new RowMapper<CartLine>() {
        @Override
        public CartLine mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new CartLine(
                    new Product(
                            rs.getInt("produit_id"),
                            rs.getString("name"),
                            rs.getInt("price"),
                            rs.getString("description")),
                    rs.getInt("quantity"));
        }
    };
    @Override
    public List<CartLine> findAll() {
        String sql = "select * from cart_lines left join produits p on p.id = cart_Lines.produit_id";
        return jdbcTemplate.query(sql,rowMapper);
    }
}

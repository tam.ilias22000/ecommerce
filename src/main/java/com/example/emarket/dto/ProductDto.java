package com.example.emarket.dto;

import com.example.emarket.entity.Categorie;
import com.example.emarket.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

public class ProductDto {

    private Integer productId;
    private String productName;
    private Integer productPrice;
    private String productDescription;
    private String categoryName;


    public static ProductDto fromDomain (Product product){
        ProductDto productDto = new ProductDto();
        productDto.setProductId(product.getId());
        productDto.setProductName(product.getName());
        productDto.setProductPrice(product.getPrice());
        productDto.setProductDescription(product.getDescription());
        productDto.setCategoryName(product.getCategorie().getName());
        return productDto;
    }

    public static Product toDomain(ProductDto productDto) {
        Categorie newCategorie = new Categorie();
        newCategorie.setName(productDto.getCategoryName());
        Product product = new Product(
                productDto.getProductId(),
                productDto.getProductName(),
                productDto.getProductPrice(),
                productDto.getProductDescription(),
                newCategorie

        );

        return product;
    }
}

package com.example.emarket.dto;

import com.example.emarket.entity.Categorie;
import com.example.emarket.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data

public class CategorieDto {

    private Integer id;
    private String name;
    private List<ProductDto> productDtos = new ArrayList<>();

    public static CategorieDto fromDomain (Categorie categorie){
        CategorieDto categorieDto = new CategorieDto();
        categorieDto.setId(categorie.getId());
        categorieDto.setName(categorie.getName());

        List<ProductDto> newPoductDto = new ArrayList<>();
        for (Product product : categorie.getProducts()) {
            newPoductDto.add(ProductDto.fromDomain(product));
        }
        categorieDto.setProductDtos(newPoductDto);

        return categorieDto;
    }

    public  Categorie toDomain(){
        Categorie categorie = new Categorie();
        categorie.setId(this.id);
        categorie.setName((this.name));

        return categorie;
    }
}

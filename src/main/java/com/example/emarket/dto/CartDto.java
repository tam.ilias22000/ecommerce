package com.example.emarket.dto;

import com.example.emarket.entity.Cart;
import com.example.emarket.entity.CartLine;
import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class CartDto {

    private Integer id;
    private List<CartLineDto> cartLineSet = new ArrayList<>();
    private Instant dateDeCreation ;

    public static CartDto fromDomain (Cart cart){
        CartDto cartDto = new CartDto();
        cartDto.setId(cart.getId());

        List<CartLineDto> cartLineDtos  = new ArrayList<>();
        for (CartLine line : cart.getCartLineSet()) {
            cartLineDtos.add(CartLineDto.fromDomain(line));
        }
        cartDto.setCartLineSet(cartLineDtos);


        cartDto.setDateDeCreation(cart.getDateDeCreation());
        return cartDto;
    }

    public  Cart toDomain(){
        Cart cart = new Cart();
        cart.setId(this.id);

        cart.setDateDeCreation(this.dateDeCreation);



        return cart;
    }
}

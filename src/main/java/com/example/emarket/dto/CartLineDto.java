package com.example.emarket.dto;

import com.example.emarket.entity.CartLine;
import com.example.emarket.entity.Product;
import lombok.Data;

@Data
public class CartLineDto {

    private Integer productIdd;
    private String productName;
    //private ProductDto productDto;
    private Integer quantity;

    public static CartLineDto fromDomain(CartLine cartLine){
        CartLineDto cartLineDto = new CartLineDto();
        cartLineDto.setProductIdd(cartLine.getProduct().getId());
        cartLineDto.setProductName(cartLine.getProduct().getName());
        //cartLineDto.setProductDto(ProductDto.fromDomain(cartLine.getProduct()));
        cartLineDto.setQuantity(cartLine.getQuantity());
        return cartLineDto;
    }
}
